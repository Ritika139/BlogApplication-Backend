package com.example.BlogBackend.modal;

import javax.persistence.*;

@Entity
public class Follow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long followid;
    @ManyToOne
    private Users follower;
    @ManyToOne
    private Users following;

    public long getFollowid() {
        return followid;
    }

    public void setFollowid(long followid) {
        this.followid = followid;
    }

    public Users getFollower() {
        return follower;
    }

    public void setFollower(Users follower) {
        this.follower = follower;
    }

    public Users getFollowing() {
        return following;
    }

    public void setFollowing(Users following) {
        this.following = following;
    }
}
