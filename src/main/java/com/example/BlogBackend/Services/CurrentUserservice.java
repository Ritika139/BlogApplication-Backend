package com.example.BlogBackend.Services;

import com.example.BlogBackend.Repository.userRepository;
import com.example.BlogBackend.modal.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

@Service
public class CurrentUserservice {
    @Autowired
    userRepository userRepository;

    public Long getUserid(Principal principal) {
        String email = principal.getName();
        Long id = userRepository.findByEmail(email).get().getUserid();
        return id;
    }

}
