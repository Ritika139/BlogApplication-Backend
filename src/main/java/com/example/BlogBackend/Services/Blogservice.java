package com.example.BlogBackend.Services;

import com.example.BlogBackend.Repository.blogRepository;
import com.example.BlogBackend.Repository.followRepository;
import com.example.BlogBackend.Repository.userRepository;
import com.example.BlogBackend.modal.Follow;
import com.example.BlogBackend.modal.Users;
import com.example.BlogBackend.modal.blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class Blogservice {

    @Autowired
    userRepository userRepository;
    @Autowired
    blogRepository blogRepository;
    @Autowired
    followRepository followRepository;

    public Optional<Users> getuserdetails(Long userid) {
//        Optional<Users> users = userRepository.findById(userid);
        return userRepository.findById(userid);
    }

    public String addnewblog(blog blog, Long userid) {
        Optional<Users> user = userRepository.findById((userid));
        blog.setUsers(user.get());
        blog.setDate(new Date());
        blog.setAccess("public");
        blogRepository.save(blog);
        return "\"Successfully added\"";
    }

    public List<blog> showmyblogs(Long userid){
        Optional<Users> user = userRepository.findById((userid));
            return blogRepository.findByUsers(user);
    }
    public List<blog> showuserblogs(Long userid){
        Optional<Users> user = userRepository.findById((userid));
        return blogRepository.findByUsers(user);
    }

    public String addfollower(Long userid, Long follower_id) {
        Optional<Users> user = userRepository.findById((userid));
        Optional<Users> user1 = userRepository.findById((follower_id));
        Follow follow = new Follow();
        follow.setFollower(user.get());
        follow.setFollowing(user1.get());
        followRepository.save(follow);
            return "\"Successfully added follower\"";
    }
    public List<Follow> showmyfollowers(Long userid){
        Optional<Users> user = userRepository.findById((userid));
        return followRepository.findByFollower(user);
    }

}
