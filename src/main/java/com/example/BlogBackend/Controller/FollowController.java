package com.example.BlogBackend.Controller;


import com.example.BlogBackend.Services.Blogservice;
import com.example.BlogBackend.Services.CurrentUserservice;
import com.example.BlogBackend.modal.Follow;
import com.example.BlogBackend.modal.blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin()
@RestController
@RequestMapping("/follow")
public class FollowController {

    @Autowired
    Blogservice blogService;
    @Autowired
    CurrentUserservice currentUserservice;


    @RequestMapping(value = "/addfollower/{userid}", method = RequestMethod.POST)
    @ResponseBody
    public String addtocart(@PathVariable Long userid,Principal principal){
        return blogService.addfollower(currentUserservice.getUserid(principal),userid);
    }
    @RequestMapping(value = "/showmyfollower", method = RequestMethod.GET)
    @ResponseBody
    public List<Follow> showmyfollowers(Principal principal) {
        return blogService.showmyfollowers(currentUserservice.getUserid(principal));
    }
}
