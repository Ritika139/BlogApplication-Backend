package com.example.BlogBackend.Controller;


import com.example.BlogBackend.Repository.blogRepository;
import com.example.BlogBackend.Repository.followRepository;
import com.example.BlogBackend.Repository.userRepository;
import com.example.BlogBackend.Services.Blogservice;
import com.example.BlogBackend.Services.CurrentUserservice;
import com.example.BlogBackend.exception.ResourceNotFoundException;
import com.example.BlogBackend.modal.Follow;
import com.example.BlogBackend.modal.Users;
import com.example.BlogBackend.modal.blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@CrossOrigin()
@RestController
@RequestMapping("/api")
public class BlogController {

    @Autowired
    followRepository followRepository;

    @Autowired
    Blogservice blogservice;
    @Autowired
    CurrentUserservice currentUserservice;
    @Autowired
    blogRepository blogRepository;

    @Autowired
    userRepository userRepository;

//    @Autowired
//    public BlogController(Blogservice blogservice, CurrentUserservice currentUserservice){
//        this.blogservice=blogservice;
//        this.currentUserservice=currentUserservice;
//    }
//    @RequestMapping(value = "/showmyblogs/recieve", method = RequestMethod.GET)
//    @ResponseBody
//    public List<blog> showmyblogs(Principal principal){
//        return blogservice.showmyblogs( currentUserservice.getUserid(principal),principal);
//    }
    @PostMapping("/addnewblog")
    public String createblog(@Valid @RequestBody blog blog, Principal principal)
    {
        return blogservice.addnewblog(blog, currentUserservice.getUserid(principal));
    }
    @GetMapping("/find-by-id/{blogid}")
    public blog getblogById(@PathVariable(value = "blogid") Long blogid) {
        return blogRepository.findById(blogid)
                .orElseThrow(() -> new ResourceNotFoundException("Blog", "id", blogid));
    }
    @GetMapping("/showblogs")
    public List<blog> getAllblogs() {
        return blogRepository.findAll();
    }
    @RequestMapping(value = "/showmyblog/recieve", method = RequestMethod.GET)
    @ResponseBody
    public List<blog> showmyblog(Principal principal) {
        return blogservice.showmyblogs(currentUserservice.getUserid(principal));
    }
    @PutMapping("/updateblog/{id}")
    public blog updateNote(@PathVariable(value = "id") Long blogid,
                            @Valid @RequestBody blog blogDetails) {

        blog blog = blogRepository.findById(blogid)
                .orElseThrow(() -> new ResourceNotFoundException("Blog", "blog-id", blogid));

        blog.setCategory(blogDetails.getCategory());
        blog.setTitle(blogDetails.getTitle());
        blog.setDescription(blogDetails.getDescription());
        blog.setImage(blogDetails.getImage());
        blog.setAccess(blogDetails.getAccess());
        blog updatedblog = blogRepository.save(blog);
        return updatedblog;
    }
    // Delete a item
    @DeleteMapping("/delete-blog/{id}")
    public ResponseEntity<?> deleteitem(@PathVariable(value = "id") Long blogid)
    {
        blog blog = blogRepository.findById(blogid)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", blogid));

        blogRepository.delete(blog);

        return ResponseEntity.ok().build();
    }
    @RequestMapping(value = "/showuserblog/{userid}", method = RequestMethod.GET)
    @ResponseBody
    public List<blog> showuserblog(@PathVariable(value = "userid") Long userid) {
        return blogservice.showuserblogs(userid);
    }
    @GetMapping("/find-blog/{title}/{access}")
    public List<blog> getitemByprice(@PathVariable(value = "title")String title, @PathVariable(value = "access")String access) {
        return blogRepository.findByTitleContainingAndAccess(title, access);
    }
    @RequestMapping(value = "/showfollowblog/{userid}", method = RequestMethod.GET)
    @ResponseBody
    public List<blog> showAllblogs(@PathVariable(value= "userid") Long userid) {
        Optional<Users> user = userRepository.findById(userid);
        List<blog> list = new LinkedList<>();
        List<Follow> followingList = followRepository.findByFollower(user);

        for(Follow f: followingList)
        {

            List<blog> blogList = blogRepository.findAllByUsersOrderByDateDesc(f.getFollowing());
            for(blog b: blogList)
            {

                list.add(b);

            }

        }
        List<blog> b = blogRepository.findByUsers(user);
        for(blog bb: b)
        {

            list.add(bb);

        }
        return list;
    }
    @GetMapping("/find-by-category/{category}/{access}")
    public List<blog> getitemByCategory(@PathVariable(value = "category") String category,@PathVariable(value = "access")String access) {
        return blogRepository.findAllByCategoryAndAccess(category,access);
    }
}
