package com.example.BlogBackend.Controller;

import com.example.BlogBackend.Repository.blogRepository;
import com.example.BlogBackend.Repository.userRepository;
import com.example.BlogBackend.Services.Blogservice;
import com.example.BlogBackend.Services.CurrentUserservice;
import com.example.BlogBackend.exception.ResourceNotFoundException;
import com.example.BlogBackend.modal.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@CrossOrigin()
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private userRepository userRepository;

    @Autowired
    private Blogservice blogservice;

    @Autowired
    private CurrentUserservice currentUserservice;

    @Autowired
    private blogRepository blogRepository;

    @GetMapping("/get1")

    public List<Users> getallUser(Principal principal){

        return userRepository.findAllByUseridNot(currentUserservice.getUserid(principal));
    }

    @PostMapping("/adduser")
    public Users createuser(@Valid @RequestBody Users user) {
        user.setActive(1);
        user.setRole("user");
        return userRepository.save(user);
    }
    @PutMapping("/updateuser/{id}")
    public Users updateUser(@PathVariable(value = "id") Long userid,
                            @Valid @RequestBody Users userDetails) {

        Users user = userRepository.findById(userid)
                .orElseThrow(() -> new ResourceNotFoundException("User", "user-id", userid));

        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        user.setEmail(userDetails.getEmail());
        user.setPhn(userDetails.getPhn());
        user.setAddress(userDetails.getAddress());
        user.setFirstname(userDetails.getFirstname());
        user.setImage(userDetails.getImage());
        Users updateduser = userRepository.save(user);
        return updateduser;
    }
    @GetMapping("/getuserdetails")
    public Optional<Users> getuserdetails(Principal principal) {
        return blogservice.getuserdetails(currentUserservice.getUserid(principal));
    }
    @GetMapping("/find-by-id/{userid}")
    public Users getUsersById(@PathVariable(value = "userid") Long userid) {
        return userRepository.findById(userid)
                .orElseThrow(() -> new ResourceNotFoundException("user", "id", userid));
    }
}
