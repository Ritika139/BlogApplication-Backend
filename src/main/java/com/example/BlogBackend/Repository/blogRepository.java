package com.example.BlogBackend.Repository;


import com.example.BlogBackend.modal.Users;
import com.example.BlogBackend.modal.blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface blogRepository extends JpaRepository<blog,Long> {

    List<blog> findByUsers(Optional<Users>users);

    List<blog> findAllByUsersOrderByDateDesc(Users following);
    List<blog> findByTitleContainingAndAccess(String title, String access);
    List<blog> findAllByCategoryAndAccess(String category, String access);
}
