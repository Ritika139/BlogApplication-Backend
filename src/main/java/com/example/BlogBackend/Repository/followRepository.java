package com.example.BlogBackend.Repository;

import com.example.BlogBackend.modal.Follow;
import com.example.BlogBackend.modal.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface followRepository extends JpaRepository<Follow,Long> {
    List<Follow> findByFollower(Optional<Users> users);
}
