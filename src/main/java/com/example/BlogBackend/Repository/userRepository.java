package com.example.BlogBackend.Repository;

import com.example.BlogBackend.modal.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
@Repository
public interface userRepository extends JpaRepository<Users,Long>{
    Optional<Users> findByEmail(String email);
    List<Users> findAllByUseridNot(Long userid);
}
